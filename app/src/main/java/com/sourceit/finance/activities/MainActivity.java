package com.sourceit.finance.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.sourceit.finance.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity{

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        unbinder = ButterKnife.bind(this);
    }

    @OnClick(R.id.deposit_btn)
    public void callDepositActivity() {
        Intent intent = new Intent(this, DepositActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.credit_btn)
    public void callCreditActivity() {
        Intent intent = new Intent(this, CreditActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
