package com.sourceit.finance.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.finance.OnTextChangeListener;
import com.sourceit.finance.R;
import com.sourceit.finance.model.Credit;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditActivity extends AppCompatActivity{

    @BindView(R.id.sum_credit_edit) EditText sumCreditEdit;
    @BindView(R.id.percent_credit_edit) EditText percentCreditEdit;
    @BindView(R.id.month_credit_edit) EditText monthCreditEdit;
    @BindView(R.id.insurance_credit_edit) EditText insuranceCreditEdit;
    @BindView(R.id.pre_credit_edit) EditText preCreditEdit;
    @BindView(R.id.tax_credit_edit) EditText taxCreditEdit;

    @BindView(R.id.initial_fee) TextView initialFeeView;
    @BindView(R.id.month_payment) TextView monthPaymentView;
    @BindView(R.id.overpayment) TextView overpaymentView;
    @BindView(R.id.total_sum) TextView totalSumView;

    Credit credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit);
        ButterKnife.bind(this);

        credit = new Credit(0, 0, 0, 0, 0, 0);

        sumCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.sum = (!sumCreditEdit.getText().toString().equals("")) ? Float.valueOf(sumCreditEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        percentCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setPercent((!percentCreditEdit.getText().toString().equals("")) ? Float.valueOf(percentCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        monthCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.month = (!monthCreditEdit.getText().toString().equals("")) ? Float.valueOf(monthCreditEdit.getText().toString()) : 0;
                showChanges();
            }
        });

        insuranceCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setInsurance((!insuranceCreditEdit.getText().toString().equals("")) ? Float.valueOf(insuranceCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        preCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setPre((!preCreditEdit.getText().toString().equals("")) ? Float.valueOf(preCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });

        taxCreditEdit.addTextChangedListener(new OnTextChangeListener() {
            @Override
            public void afterTextChanged(Editable s) {
                credit.setTax((!taxCreditEdit.getText().toString().equals("")) ? Float.valueOf(taxCreditEdit.getText().toString()) : 0);
                showChanges();
            }
        });
    }

    private void showChanges() {
        initialFeeView.setText(String.format(Locale.US, "%.2f",  credit.getInitialFee()));
        monthPaymentView.setText(String.format(Locale.US, "%.2f", credit.getMonthPayment()));
        overpaymentView.setText(String.format(Locale.US, "%.2f", credit.getOverPayment()));
        totalSumView.setText(String.format(Locale.US, "%.2f", credit.getTotalSum()));
    }
}

